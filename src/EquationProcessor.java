import exceptions.InvalidCharecter;

import java.util.Scanner;

public class EquationProcessor {
    private int position, currentChar;
    private final String equation;

    public EquationProcessor(String equation) {
        this.position = -1;
        this.equation = equation;
    }

    public double calculate() {
        return evaluate();
    }

    private void movePointer() {
        if (++position < equation.length())
            currentChar = equation.charAt(position);
        else
            currentChar = -1;
    }

    private boolean readTo(char charToRead) {
        while (isCurrentASpace())
            movePointer();
        if (currentChar == charToRead) {
            movePointer();
            return true;
        }
        return false;
    }

    private boolean isCurrentASpace() {
        return currentChar == ' ';
    }

    private double evaluate() {
        movePointer();
        double result = evaluateExpression();
        if (position < equation.length())
            throw new InvalidCharecter("Unexpected: " + (char) currentChar);
        return result;
    }

    private double evaluateExpression() {
        double result = evaluateTerm();
        boolean expressionEvaluated = false;
        while (!expressionEvaluated) {
            if (readTo('+'))
                result += evaluateTerm();
            else if (readTo('-'))
                result -= evaluateTerm();
            else
                expressionEvaluated = true;
        }
        return result;
    }

    private double evaluateTerm() {
        double result = evaluateFactor();
        boolean termEvaluated = false;
        while (!termEvaluated) {
            if (readTo('*'))
                result *= evaluateFactor();
            else if (readTo('/'))
                result /= evaluateFactor();
            else
                termEvaluated = true;
        }

        return result;
    }

    private double evaluateFactor() {
        if (readTo('+'))
            return evaluateFactor();
        if (readTo('-'))
            return -evaluateFactor();

        return evaluateNext();
    }

    private double evaluateNext() {
        double result;
        int startPosition = this.position;
        if (readTo('(')) {
            result = evaluateExpression();
            readTo(')');
        } else if ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') {
            result = evaluateNumber(startPosition);
        } else if (currentChar >= 'A' && currentChar <= 'z') {
            result = evaluateVar(startPosition);
        } else {
            throw new InvalidCharecter("Unexpected: " + (char) currentChar);
        }

        if (readTo('^'))
            result = Math.pow(result, evaluateFactor());

        return result;
    }

    private double evaluateNumber(int startPosition) {
        while ((currentChar >= '0' && currentChar <= '9') || currentChar == '.')
            movePointer();
        return Double.parseDouble(equation.substring(startPosition, this.position));
    }

    private double evaluateVar(int startPosition) {
        while (currentChar >= 'A' && currentChar <= 'z')
            movePointer();

        String variableName = equation.substring(startPosition, this.position);
        System.out.println("Enter value for variable " + variableName + " : ");
        Scanner input = new Scanner(System.in);
        return input.nextDouble();
    }


}
