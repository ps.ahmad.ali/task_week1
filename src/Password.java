import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Password {
    Random random = new Random();

    private char randomNumber() {
        return (char) (random.nextInt(10) + '0');
    }

    private char randomLetter() {
        return (char) (random.nextInt(26) + 'A');
    }

    private char randomSpecial() {
        String specialChar="_$#%";
        return specialChar.charAt(random.nextInt(specialChar.length()));
    }

    public String generate() {
        ArrayList<Character> password = new ArrayList<>();
        for (int i = 0; i < 4; i++)
            password.add(randomNumber());
        for (int i = 0; i < 2; i++)
            password.add(randomLetter());
        for (int i = 0; i < 2; i++)
            password.add(randomSpecial());
        Collections.shuffle(password);
        StringBuilder sb = new StringBuilder();
        for (Character character : password) {
            sb.append(character);
        }
        return sb.toString();
    }
}
