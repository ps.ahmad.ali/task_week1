import exceptions.EmptyException;
import exceptions.OverflowExeptions;

public class Stack {
    private int size;
    private int[] stack;
    private int count = 0;
    private final boolean dynamic;

    public Stack() {
        dynamic = true;
        size = 0;
        stack = new int[size];
    }

    public Stack(int stackSize) {
        dynamic = false;
        size = stackSize;
        stack = new int[size];
    }

    private void expand() {
        int[] newStack = new int[size + 1];
        if (size >= 0)
            System.arraycopy(stack, 0, newStack, 0, size);
        size++;
        stack = newStack;
    }

    private void contract() {
        int[] newStack = new int[size - 1];
        if (size >= 0)
            System.arraycopy(stack, 0, newStack, 0, size - 1);
        size--;
        count--;
        stack = newStack;
    }

    public void push(int element) {
        if (isFull()) {
            if (isFixed())
                throw new OverflowExeptions();
            expand();
        }
        stack[count++] = element;
    }

    private boolean isFull() {
        return count == size;
    }

    private boolean isFixed() {
        return !dynamic;
    }

    public int pop() {
        int popped = peek();
        if (dynamic) {
            contract();
        } else {
            count--;
        }
        return popped;

    }

    public int getSize() {
        return count;
    }

    public int peek() {
        if (count == 0)
            throw new EmptyException("Stack is empty");
        return (stack[count - 1]);
    }
}