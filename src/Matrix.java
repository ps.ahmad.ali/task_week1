import exceptions.InvalidInputException;

public class Matrix {

    private static boolean isValidMatrix(double[][] matrix) {
        // TODO to check null rows
        for (double[] doubles : matrix)
            if (doubles == null || doubles.length != matrix[0].length)
                return false;
        return true;
    }

    public static double[][] sum(double[][] matrix1, double[][] matrix2) {
        if (!isValidMatrix(matrix1) || !isValidMatrix(matrix2))
            throw new InvalidInputException("Not a valid matrix");
        if (!isValidForSum(matrix1, matrix2)) {
            throw new InvalidInputException("The 2 matrices should be the same size");
        }
        double[][] result = new double[matrix1.length][matrix1[0].length];
        for (int row = 0; row < matrix1.length; row++)
            for (int col = 0; col < matrix1[row].length; col++)
                result[row][col] = matrix1[row][col] + matrix2[row][col];
        return result;
    }

    private static boolean isValidForSum(double[][] matrix1, double[][] matrix2) {
        return matrix1.length == matrix2.length && matrix1[0].length == matrix2[0].length;
    }

    public static double[][] scale(double[][] matrix, double multiplier) {
        if (!isValidMatrix(matrix))
            throw new InvalidInputException("Not a valid matrix");
        double[][] result = new double[matrix.length][matrix[0].length];
        for (int row = 0; row < matrix.length; row++)
            for (int col = 0; col < matrix[row].length; col++)
                result[row][col] = matrix[row][col] * multiplier;
        return result;
    }

    public static double[][] transpose(double[][] matrix) {
        if (!isValidMatrix(matrix))
            throw new InvalidInputException("Not a valid matrix");
        double[][] result = new double[matrix[0].length][matrix.length];
        for (int row = 0; row < result.length; row++)
            for (int col = 0; col < result[row].length; col++)
                result[row][col] = matrix[col][row];
        return result;
    }

    public static double[][] multiply(double[][] matrix1, double[][] matrix2) {
        if (!isValidMatrix(matrix1) || !isValidMatrix(matrix2))
            throw new InvalidInputException("Not a valid matrix");
        if (matrix1[0].length != matrix2.length) {
            throw new InvalidInputException("The number of columns in the first matrix should be the same as the number of rows in the second one");
        }
        double[][] result = new double[matrix1.length][matrix2[0].length];
        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                int element = 0;
                for (int counter = 0; counter < matrix2.length; counter++)
                    element += matrix1[row][counter] * matrix2[counter][col];
                result[row][col] = element;
            }
        }
        return result;
    }

    private static boolean contains(final int[] array, final int key) {
        for (final int i : array) {
            if (i == key) {
                return true;
            }
        }
        return false;
    }

    public static double[][] subMatrix(double[][] matrix, int[] rowToRemove, int[] columnsToRemove) {
        if (!isValidMatrix(matrix))
            throw new InvalidInputException("Not a valid matrix");
        double[][] result = new double[matrix.length - rowToRemove.length][matrix[0].length - columnsToRemove.length];
        for (int row = 0, resultRow = 0; row < matrix.length; row++) {
            if (contains(rowToRemove, row + 1)) {
                continue;
            }
            for (int col = 0, resultCol = 0; col < matrix[row].length; col++) {
                if (contains(columnsToRemove, col + 1)) {
                    continue;
                }
                result[resultRow][resultCol++] = matrix[row][col];
            }
            resultRow++;
        }
        return result;
    }

    private static double[][] square(Function operation, double[][] matrix) {
        if (!isValidMatrix(matrix))
            throw new InvalidInputException("Not a valid matrix");
        if (matrix.length != matrix[0].length)
            throw new InvalidInputException("The matrix should be square");
// TODO introduce functional
        double[][] result = new double[matrix.length][matrix[0].length];
        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                if (operation.apply(row, col)) {
                    result[row][col] = matrix[row][col];
                }
            }
        }
        return result;
    }

    public static double[][] diagonal(double[][] matrix) {
        return square((row, column) -> row == column, matrix);
    }

    public static double[][] lower(double[][] matrix) {
        return square((row, column) -> row >= column, matrix);
    }

    public static double[][] upper(double[][] matrix) {
        return square((row, column) -> row <= column, matrix);
    }


    public static double determinant(double[][] matrix) {
        if (!isValidMatrix(matrix))
            throw new InvalidInputException("Not a valid matrix");
        if (!isSquare(matrix))
            throw new InvalidInputException("The matrix should be square");

        return calculateDet(matrix);

    }

    private static double calculateDet(double[][] matrix) {
        if (matrix.length == 2)
            return ((matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]));
        double det = 0;
        int sign = 1;
        for (int column = 0; column < matrix[0].length; column++) {
            det += (sign *= -1) * matrix[0][column] * calculateDet(subMatrix(matrix,
                    new int[]{1},
                    new int[]{column + 1}));
        }
        return det;// TODO fix this by avoid keep checking if square and valid
    }

    private static boolean isSquare(double[][] matrix) {
        return matrix.length == matrix[0].length;
    }

    public static void printMatrix(double[][] matrixToPrint) {
        for (double[] doubles : matrixToPrint) {
            for (double aDouble : doubles)
                System.out.print(aDouble + " ");
            System.out.println();
        }
        System.out.println();
    }

    private interface Function {
        boolean apply(int row, int column);
    }
}