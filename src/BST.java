import exceptions.InvalidInputException;

public class BST {
    // TODO read about state design pattern and try to apply it
    abstract static class State {
        public abstract boolean accept(int data);

    }

    class nullState extends State {
        @Override
        public boolean accept(int data) {
            root = new Node(data);
            changeStateToNotNull();
            return true;
        }

        private void changeStateToNotNull() {
            state = new notNullState();
        }
    }

    class notNullState extends State {
        @Override
        public boolean accept(int data) {
            return root.accept(data);
        }
    }

    public Node root;
    public State state;

    public BST() {
        root = null;
        state = new nullState();
    }

    public boolean accept(int data) {
        return state.accept(data);
    }

    public int treeDepth() {
        return findDepth(root);
    }

    private int findDepth(Node parent) {
        if (parent == null)
            return 0;
        int leftDepth = findDepth(parent.left);
        int rightDepth = findDepth(parent.right);
        return (Math.max(rightDepth, leftDepth) + 1);
    }

    public int depth(int valueToFind) {
        int depthValue = nodeDepth(root, valueToFind);
        if (depthValue == -1)
            throw new InvalidInputException("Value not in tree");
        return depthValue;

    }

    private int nodeDepth(Node parent, int valueToFind) {
        if (parent == null)
            return -1;
        int distance = -1;
        if ((parent.item == valueToFind) ||
                (distance = nodeDepth(parent.left, valueToFind)) >= 0 ||
                (distance = nodeDepth(parent.right, valueToFind)) >= 0)
            return distance + 1;
        return distance;
    }

}

class Node {
    int item;
    Node left;
    Node right;

    public Node(int data) {
        item = data;
        left = null;
        right = null;
    }

    public boolean accept(int data) {
        if (data == item)
            return false;
        if (data < item) {
            return acceptToLeft(data);
        }
        return acceptToRight(data);
    }

    private boolean acceptToRight(int data) {
        if (right == null) {
            right = new Node(data);
            return true;
        }
        return right.accept(data);
    }

    private boolean acceptToLeft(int data) {
        if (left == null) {
            left = new Node(data);
            return true;
        }
        return left.accept(data);
    }
}
