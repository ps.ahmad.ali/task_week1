import exceptions.EmptyException;
import exceptions.OverflowExeptions;

public class Queue {
    private int size;
    private int[] queue;
    private int count = 0;
    private final boolean dynamic;

    public Queue() {
        dynamic = true;
        size = 0;
        queue = new int[size];
    }

    public Queue(int queueSize) {
        dynamic = false;
        size = queueSize;
        queue = new int[size];
    }

    private void expand() {
        int[] newQueue = new int[size + 1];
        if (size >= 0)
            System.arraycopy(queue, 0, newQueue, 0, size);
        size++;
        queue = newQueue;
    }

    private void contract() {
        int[] newQueue = new int[size - 1];
        if (size >= 0)
            System.arraycopy(queue, 1, newQueue, 0, size - 1);
        size--;
        count--;
        queue = newQueue;
    }

    public void enque(int element) {
        if (isFull()) {
            if (isFixed())
                throw new OverflowExeptions();
            expand();
        }
        queue[count++] = element;
    }

    private boolean isFixed() {
        return !dynamic;
    }

    private boolean isFull() {
        return count == size;
    }

    public int deque() {
        int dequeued = peek();
        if (dynamic)
            contract();
        else {
            System.arraycopy(queue, 1, queue, 0, count - 1);
            count--;
        }
        return dequeued;
    }

    public int getSize() {
        return count;
    }

    public int peek() {
        if (count == 0)
            throw new EmptyException("Queue is empty");
        return (queue[0]);
    }

}
